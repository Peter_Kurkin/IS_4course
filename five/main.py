import os

import pandas as pd
from pymystem3 import Mystem
from sklearn.metrics.pairwise import cosine_similarity
from collections import Counter
import math

from four.main import compute_tf, compute_idf, compute_tf_idf

"""
1. Преобразование запроса пользователя в вектор:
    - Разбейте запрос пользователя на слова.
    - Примените лемматизацию к каждому слову.
    - Вычислите TF для каждого слова запроса.
    - Вычислите TF-IDF для каждого слова запроса, используя ранее вычисленные IDF значения.
    
2. Вычисление вектора TF-IDF для каждого документа в индексе:
    - У вас уже должны быть вычислены значения TF-IDF для каждого слова в каждом документе.
    
3. Ранжирование документов:
    - Для каждого документа вычислите косинусное сходство между вектором запроса и вектором документа.
    - Отсортируйте документы по убыванию их сходства с запросом.
    
4. Вывод результата:
    - Выведите топ-N наиболее релевантных документов.
"""

mystem = Mystem()

def query_to_vector(query, idf, total_docs):
    """
    Преобразует запрос в вектор TF-IDF, с применением лемматизации к каждому слову.
    """
    words = mystem.lemmatize(query.lower())
    words = [word for word in words if word.strip() and word not in [' ', '\n']]
    word_count = Counter(words)
    query_length = len(words)
    query_vector = {}

    for word, count in word_count.items():
        tf = count / query_length
        word_idf = idf.get(word, math.log10(total_docs / 1))  # Если слово не в IDF, используем минимальный IDF
        query_vector[word] = tf * word_idf

    return query_vector


def compute_cosine_similarity(doc_vector, query_vector):
    """
    Вычисляет косинусное сходство между двумя векторами.
    """
    # Преобразование векторов в формат, подходящий для sklearn
    doc_items = list(doc_vector.items())
    query_items = list(query_vector.items())
    all_words = list(set(doc_vector.keys()).union(set(query_vector.keys())))
    doc_vector = [doc_vector.get(word, 0) for word in all_words]
    query_vector = [query_vector.get(word, 0) for word in all_words]

    return cosine_similarity([doc_vector], [query_vector])[0][0]


def vector_search_multi(queries, tf_idf, idf, total_docs, output_txt_file, output_csv_file):
    results = []
    with open(output_txt_file, 'w') as txt_file:
        for query in queries:
            query_vector = query_to_vector(query, idf, total_docs)
            scores = {}

            for doc, doc_vector in tf_idf.items():
                scores[doc] = compute_cosine_similarity(doc_vector, query_vector)

            sorted_scores = sorted(scores.items(), key=lambda x: x[1], reverse=True)
            txt_file.write(f"Запрос: '{query}'\n")
            for doc, score in sorted_scores[:10]:  # Меняйте N для разного количества документов
                result_line = f"Документ: {doc}, Счет: {score}\n"
                txt_file.write(result_line)
                results.append({"Query": query, "Document": doc, "Score": score})
            txt_file.write("\n")

    # Создание DataFrame и сохранение в CSV
    results_df = pd.DataFrame(results)
    results_df.to_csv(output_csv_file, index=False)

    print(f"Результаты поиска сохранены в {output_txt_file} и {output_csv_file}.")


if __name__ == '__main__':
    # Путь к папке с текстовыми файлами
    path = '/mnt/c/Work/IS_4course/two/result'
    # Чтение файлов
    corpus = {}
    for file in os.listdir(path):
        with open(os.path.join(path, file), 'r') as f:
            corpus[file] = f.read().split()

    # Вычисление TF для каждого документа
    tf = {doc: compute_tf(text) for doc, text in corpus.items()}

    # Вычисление IDF для всего корпуса
    idf = compute_idf(corpus)

    # Вычисление TF-IDF для каждого документа
    tf_idf = compute_tf_idf(tf, idf)

    # Предполагаем, что tf_idf и idf уже вычислены, и total_docs - общее количество документов
    queries = ["A", "A экспедиция", "A экспедиция аннуляция"]
    output_txt = 'search_results.txt'
    output_csv = 'search_results.csv'
    vector_search_multi(queries, tf_idf, idf, len(corpus), output_txt, output_csv)
