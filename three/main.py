import os
import re
from collections import defaultdict
from multiprocessing import Pool
import sys
from nltk import word_tokenize
from nltk.corpus import stopwords
from pymorphy2 import MorphAnalyzer
from pymystem3 import Mystem

# Загрузка русских стоп-слов
# try:
#     _create_unverified_https_context = ssl._create_unverified_context
# except AttributeError:
#     pass
# else:
#     ssl._create_default_https_context = _create_unverified_https_context
# nltk.download('stopwords')
# nltk.download('punkt')
russian_stopwords = set(stopwords.words('russian'))


def is_valid_token(token):
    # Регулярное выражение для проверки, что токен является словом (только буквы и, возможно, дефисы)
    return re.match(r'^[А-Яа-яЁёA-Za-z]+-?[А-Яа-яЁёA-Za-z]*$', token) is not None


def process_file_mp(filename, directory):
    # Similar file processing logic as before
    # Assuming necessary imports and variables (like mystem) are defined
    filepath = os.path.join(directory, filename)
    file_lemmas = []  # Список для хранения всех лемм (включая повторения)

    mystem = Mystem()

    print(f'file: {filepath}')

    with open(filepath, 'r', encoding='utf-8') as file:
        text = file.read()
        tokens = word_tokenize(text, language="russian")
        lemmas = [mystem.lemmatize(word)[0] for word in tokens]

        for lemma in lemmas:
            if lemma not in russian_stopwords and is_valid_token(lemma):
                file_lemmas.append(lemma)  # Добавляем все леммы

    return filename, file_lemmas


def create_inverted_index(directory, result=None, processes=None):
    inverted_index = {}

    file_list = [filename for filename in os.listdir(directory) if filename.endswith(".txt")]

    # Using Pool for multiprocessing
    with Pool(processes=processes) as pool:
        file_lemmas_pairs = pool.starmap(process_file_mp, [(filename, directory) for filename in file_list])

    for filename, lemmas in file_lemmas_pairs:
        for lemma in lemmas:
            if lemma not in inverted_index:
                inverted_index[lemma] = []
            if filename not in inverted_index[lemma]:
                inverted_index[lemma].append(filename)  # Добавляем файл в список для данной леммы

    # Writing file-specific results after all multiprocessing tasks are complete
    if result is not None:
        for filename, lemmas in file_lemmas_pairs:
            result_filepath = os.path.join(result, filename)
            print(f'Записываю результат в файл {result_filepath}')
            with open(result_filepath, 'w', encoding='utf-8') as result_file:
                for lemma in lemmas:
                    result_file.write(f"{lemma}\n")

    return inverted_index


def boolean_search(query, inverted_index):
    words = query.split()
    mystem = Mystem()

    # Преобразование слов в их леммы и формирование выражения для eval
    eval_expression = ""
    universe = set().union(*[set(files) for files in inverted_index.values()])
    for word in words:
        if word in ["&", "|"]:
            eval_expression += f" {word} "
        else:
            # Если у слова есть приписка НЕ (!), то берём весь словарь целиком (universe) и вычитаем оттуда наш
            if word[0] == "!":
                lemma = mystem.lemmatize(word[1:])[0]
                if lemma in inverted_index:
                    eval_expression += f"(universe - set(inverted_index['{lemma}']))"
                else:
                    eval_expression += "universe"
            else:
                lemma = mystem.lemmatize(word)[0]
                # Добавляем множество файлов для леммы, если она есть в индексе, иначе пустое множество
                if lemma in inverted_index:
                    eval_expression += f"set(inverted_index['{lemma}'])"
                else:
                    eval_expression += "set()"

    # Выполнение логического выражения
    try:
        result_docs = eval(eval_expression)
    except SyntaxError:
        result_docs = set()

    return result_docs



def save_results_to_file(inverted_index, queries, file_path):
    with open(file_path, 'w', encoding='utf-8') as file:
        # Вывод инвертированного индекса
        file.write("Инвертированный Индекс:\n")
        for word, docs in inverted_index.items():
            file.write(f"\t{word}: {', '.join(docs)}\n")

        # Вывод результатов булева поиска
        file.write("\nРезультаты Булева Поиска:\n")
        for query in queries:
            result = boolean_search(query, inverted_index)
            file.write(f"Запрос: {query}\n")
            file.write(f"\tРезультат: {', '.join(result) if result else 'Нет документов'}\n\n")


inverted_index = create_inverted_index(directory='/mnt/c/Work/IS_4course/one/result',
                                       result='/mnt/c/Work/IS_4course/two/result')
file_path = '/mnt/c/Work/IS_4course/three/search_results.txt'
queries = [
    "вариация & неоднократный | употребление",
    "вариация & !неоднократный | !употребление",
    "вариация | неоднократный | употребление",
    "вариация | !неоднократный | !употребление",
    "вариация & неоднократный & употребление"
]

save_results_to_file(inverted_index, queries, file_path)
