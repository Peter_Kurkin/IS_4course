import os
import re
import ssl
import time
# from pymorphy2 import MorphAnalyzer
from collections import defaultdict
from multiprocessing import Pool

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from pymystem3 import Mystem

# Загрузка русских стоп-слов
# try:
#     _create_unverified_https_context = ssl._create_unverified_context
# except AttributeError:
#     pass
# else:
#     ssl._create_default_https_context = _create_unverified_https_context
# nltk.download('stopwords')
# nltk.download('punkt')
russian_stopwords = set(stopwords.words('russian'))


def is_valid_token(token):
    # Регулярное выражение для проверки, что токен является словом (только буквы и, возможно, дефисы)
    return re.match(r'^[А-Яа-яЁёA-Za-z]+-?[А-Яа-яЁёA-Za-z]*$', token) is not None


def process_file_mp(filename, directory):
    # Similar file processing logic as before
    # Assuming necessary imports and variables (like mystem) are defined
    filepath = os.path.join(directory, filename)
    file_lemmas = []  # Список для хранения всех лемм (включая повторения)

    mystem = Mystem()

    print(f'file: {filepath}')

    with open(filepath, 'r', encoding='utf-8') as file:
        text = file.read()
        tokens = word_tokenize(text, language="russian")
        lemmas = [mystem.lemmatize(word)[0] for word in tokens]

        for lemma in lemmas:
            if lemma not in russian_stopwords and is_valid_token(lemma):
                file_lemmas.append(lemma)  # Добавляем все леммы

    return filename, file_lemmas


def create_inverted_index(directory, result=None, processes=None):
    inverted_index = {}

    file_list = [filename for filename in os.listdir(directory) if filename.endswith(".txt")]

    # Using Pool for multiprocessing
    with Pool(processes=processes) as pool:
        file_lemmas_pairs = pool.starmap(process_file_mp, [(filename, directory) for filename in file_list])

    for filename, lemmas in file_lemmas_pairs:
        for lemma in lemmas:
            if lemma not in inverted_index:
                inverted_index[lemma] = []
            if filename not in inverted_index[lemma]:
                inverted_index[lemma].append(filename)  # Добавляем файл в список для данной леммы

    # Writing file-specific results after all multiprocessing tasks are complete
    if result is not None:
        for filename, lemmas in file_lemmas_pairs:
            result_filepath = os.path.join(result, filename)
            print(f'Записываю результат в файл {result_filepath}')
            with open(result_filepath, 'w', encoding='utf-8') as result_file:
                for lemma in lemmas:
                    result_file.write(f"{lemma}\n")

    return inverted_index


# def create_inverted_index(directory, result=None, processes=None):
#     """
#     :return: Dict[page_1.txt] = [word_1, word_2, ...]
#     """
#     inverted_index = {}
#
#     file_list = [filename for filename in os.listdir(directory) if filename.endswith(".txt")]
#
#     # Using Pool for multiprocessing
#     with Pool(processes=processes) as pool:
#         file_indices = pool.starmap(process_file_mp, [(filename, directory) for filename in file_list])
#
#     # Aggregating results into the main inverted_index
#     for file_index in file_indices:
#         for lemma, files in file_index.items():
#             if lemma not in inverted_index:
#                 inverted_index[lemma] = set()
#             inverted_index[lemma].update(files)
#
#     # Writing file-specific results after all multiprocessing tasks are complete
#     if result is not None:
#         for file_index, filename in zip(file_indices, file_list):
#             result_filepath = os.path.join(result, filename)
#             print(f'Записываю результат в файл {result_filepath}')
#             with open(result_filepath, 'w', encoding='utf-8') as result_file:
#                 for lemma, files in sorted(file_index.items()):
#                     result_file.write(f"{lemma}\n")
#
#     return inverted_index


time = time.time()
inverted_index = create_inverted_index(directory='/mnt/c/Work/IS_4course/one/result',
                                       result='/mnt/c/Work/IS_4course/two/result')

# Вывод инвертированного индекса
print(inverted_index)
