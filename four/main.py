import os
import math
from collections import defaultdict

import pandas as pd

# Путь к папке с текстовыми файлами
path = '/mnt/c/Work/IS_4course/two/result'


def compute_tf(text):
    """
    TF (Term Frequency) - частота термина, показывает, насколько часто термин встречается в документе.
    Вычисляется как отношение числа вхождений термина к общему количеству слов в документе.
    """
    tf_text = defaultdict(int)
    for word in text:
        if word != '-----------------------':  # Исключаем разделитель
            tf_text[word] += 1
    word_count = sum(tf_text.values())
    return {word: round(count / word_count, 6) for word, count in tf_text.items()}


def compute_idf(corpus):
    """
    IDF (Inverse Document Frequency) - обратная документная частота, используется для оценки "важности" слова во всем корпусе документов.
    Вычисляется как логарифм отношения общего числа документов к числу документов, содержащих данный термин.
    """
    idf_text = defaultdict(lambda: 0)
    total_docs = len(corpus)
    for text in corpus.values():
        for word in set(text):
            if word != '-----------------------':
                idf_text[word] += 1
    return {word: round(math.log10(total_docs / count), 6) for word, count in idf_text.items()}


def compute_tf_idf(tf, idf):
    """
    TF-IDF (Term Frequency-Inverse Document Frequency) - произведение TF и IDF.
    Используется для определения важности слова в конкретном документе относительно других документов в корпусе.
    Большие значения TF-IDF указывают на большую важность слова.
    """
    tf_idf_text = {}
    for doc, tf_vals in tf.items():
        tf_idf_text[doc] = {word: round(tf_val * idf[word], 6) for word, tf_val in tf_vals.items()}
    return tf_idf_text


if __name__ == '__main__':
    # Чтение файлов
    corpus = {}
    for file in os.listdir(path):
        with open(os.path.join(path, file), 'r') as f:
            corpus[file] = f.read().split()

    # Вычисление TF для каждого документа
    tf = {doc: compute_tf(text) for doc, text in corpus.items()}

    # Вычисление IDF для всего корпуса
    idf = compute_idf(corpus)

    # Вычисление TF-IDF для каждого документа
    tf_idf = compute_tf_idf(tf, idf)

    # Вывод результатов
    print("TF (Term Frequency):")
    for doc, tf_vals in sorted(tf.items()):
        print(f"{doc}: {', '.join([f'{word}: {freq}' for word, freq in tf_vals.items()])}")

    print("\nIDF (Inverse Document Frequency):")
    for word, freq in sorted(idf.items()):
        print(f"{word}: {freq}")

    print("\nTF-IDF:")
    for doc, tf_idf_vals in sorted(tf_idf.items()):
        print(f"{doc}: {', '.join([f'{word}: {freq}' for word, freq in tf_idf_vals.items()])}")

    # Запись результатов в файл
    with open(file='./result.txt', mode='w') as file:
        file.write("TF (Term Frequency):\n")
        for doc, tf_vals in sorted(tf.items()):
            file.write(f"{doc}: {', '.join([f'{word}: {freq}' for word, freq in tf_vals.items()])}\n")

        file.write("\nIDF (Inverse Document Frequency):\n")
        for word, freq in sorted(idf.items()):
            file.write(f"{word}: {freq}\n")

        file.write("\nTF-IDF:\n")
        for doc, tf_idf_vals in sorted(tf_idf.items()):
            file.write(f"{doc}: {', '.join([f'{word}: {freq}' for word, freq in tf_idf_vals.items()])}\n")

    # Создание DataFrame для TF, IDF и TF-IDF
    tf_df = pd.DataFrame(tf).fillna(0).round(6)
    idf_df = pd.DataFrame(idf.items(), columns=['Word', 'IDF']).set_index('Word')
    tf_idf_records = []
    for doc, words in tf_idf.items():
        for word, value in words.items():
            tf_idf_records.append({'Document': doc, 'Word': word, 'TF-IDF': value})
    tf_idf_df = pd.DataFrame(tf_idf_records)

    # Преобразование в таблицу, где строки - слова, столбцы - документы, значения - TF-IDF
    tf_idf_df_pivot = tf_idf_df.pivot(index='Word', columns='Document', values='TF-IDF').fillna(0).round(6)

    # Сохранение в CSV
    tf_df.to_csv('tf_utf_8.csv', encoding='utf-8')
    idf_df.to_csv('idf_utf_8.csv', encoding='utf-8')
    tf_idf_df_pivot.to_csv('tf_idf_utf_8.csv', encoding='utf-8')

    tf_df.to_csv('tf_exel.csv', encoding='cp1251')
    idf_df.to_csv('idf_exel.csv', encoding='cp1251')
    tf_idf_df_pivot.to_csv('tf_idf_exel.csv', encoding='cp1251')

    print("CSV файлы успешно сохранены.")
