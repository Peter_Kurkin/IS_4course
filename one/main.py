import random
import time

import requests
from bs4 import BeautifulSoup
import os


def crawl_and_save(pages_to_visit: list[str], max_pages=100, min_words=1000):
    visited = set()
    page_count = 0

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
    }

    while pages_to_visit and page_count < max_pages:

        current_url = pages_to_visit.pop(0)
        if current_url in visited:
            continue

        try:
            response = requests.get(current_url, headers=headers)
            soup = BeautifulSoup(response.content, 'html.parser')
            text = soup.get_text().strip()
            word_count = len(text.split())

            if word_count >= min_words:
                print(f'Downloading: {current_url}')

                with open(f"result/page_{page_count}.txt", "w", encoding="utf-8") as file:
                    file.write(text)

                with open("index.txt", "a", encoding="utf-8") as index_file:
                    index_file.write(f"{page_count}: {current_url}\n")

                page_count += 1

            else:
                print(f'Small simdol {word_count}<{min_words}: {current_url}')

            for link in soup.find_all('a'):
                href = link.get('href')
                if href and href.startswith('http'):
                    pages_to_visit.append(href)

            visited.add(current_url)

            # Добавление случайной задержки
            # time.sleep(random.uniform(1, 3))

        except Exception as e:
            print(f"Error crawling {current_url}: {e}")

    print(f"Crawled {page_count} pages.")


# crawl_and_save(pages_to_visit=["https://dzen.ru/", "https://ya.ru/"])
crawl_and_save(pages_to_visit=["https://mybook.ru/catalog/books/free/"])
