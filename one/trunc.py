import os
import re

# Путь к директории, где находятся файлы
directory_path = "/Users/patrick/Documents/PycharmProjects/IS_4course/one/result"

# Перебираем все файлы в директории
for filename in os.listdir(directory_path):
    if filename.endswith(".txt"):  # проверяем, что файл является текстовым
        file_path = os.path.join(directory_path, filename)

        # Чтение содержимого файла
        with open(file_path, 'r') as file:
            content = file.read()

        # Замена множественных переносов строк на один
        content = re.sub(r'\n+', '\n', content)

        # Перезапись файла с обновленным содержимым
        with open(file_path, 'w') as file:
            file.write(content)
